from thread import *
import logging
import traceback
import time

class Client(object):
    """description of class"""
    def __init__(self, server, conn, addr, callback, onLost):
        self.server = server
        self.conn = conn
        self.addr = addr
        self.callback = callback
        self.onLost = onLost

        self.connected = True
        self.buffer = ""

        start_new_thread(self._readerloop ,())

    def _readerloop(self):
        # infinite loop so that function do not terminate and thread do not end.
        while self.connected:
         
            # Receiving from client
            data = self.conn.recv(1024)

            if not data:
                self.connected = False
                self.server.clients.remove(self)
                if self.onLost:
                    self.onLost(self)
                break

            if self.callback:
                try:
                    self.callback(self, data)
                except SystemExit as ex:
                    self.close()
                except Exception as ex:
                    print str(ex) # traceback.format_exc()
                    #logging.exception("Exception in data-callback", traceback.format_exc())
            else:
                self.buffer = data
        self.conn.close()

    def read(self, timeout):
        for n in range(timeout):
            if len(self.buffer) > 0:
                tmp = self.buffer
                self.buffer = ""
                return tmp
            time.sleep(0.01)
        return None

    def send(self, data):
        if self.connected:
            self.conn.sendall(data)

    def sendCommand(self, command):
        self.send(command + '\r\n')#'"\n")

    def setCallback(self, callback):
        self.callback = callback

    def setOnLoast(self, onLost):
        self.onLost = onLost

    def close(self):
        self.conn.close()