class ConsoleBuffer(object):
    """
    >>> cb = ConsoleBuffer()
    >>> for c in 'Hallo Welt': cb.append(c)
    >>> cb.getBuffer()
    (10, 'Hallo Welt')
    """
    def __init__(self):
        self.buffer = []
        self.pos = 0

    def fixPos(self):
        if self.pos < 0:
            self.pos = 0
            return# False
        elif self.pos > len(self.buffer):
            self.pos = len(self.buffer) - 1 if len(self.buffer) > 0 else 0
            return# False
        return# True

    def doLeft(self):
        """
        >>> cb = ConsoleBuffer()
        >>> for c in 'Hallo Welt': cb.append(c)
        >>> for n in range(100): cb.doLeft()
        >>> cb.getBuffer()
        (0, 'Hallo Welt')
        """
        self.pos -= 1
        return self.fixPos()

    def doRight(self):
        """
        >>> cb = ConsoleBuffer()
        >>> for c in 'Hallo Welt': cb.append(c)
        >>> cb.pos = 0
        >>> for n in range(100): cb.doRight()
        >>> cb.getBuffer()
        (10, 'Hallo Welt')
        """
        self.pos += 1
        return self.fixPos()

    def doBackspace(self):
        """
        >>> cb = ConsoleBuffer()
        >>> for c in 'Hallo Welt': cb.append(c)
        >>> cb.pos = 5
        >>> cb.doBackspace()
        >>> cb.getBuffer()
        (4, 'Hall Welt')
        >>> cb.pos = 9
        >>> cb.doBackspace()
        >>> cb.getBuffer()
        (8, 'Hall Wel')
        >>> cb.pos = 0
        >>> cb.doBackspace()
        >>> cb.getBuffer()
        (0, 'Hall Wel')
        """
        self.fixPos()
        if self.pos == 0:
            return# False
        if len(self.buffer) == 0:
            return# False

        del self.buffer[self.pos - 1]
        self.pos -= 1
        return# True

    def doDelete(self):
        """
        >>> cb = ConsoleBuffer()
        >>> for c in 'Hallo Welt': cb.append(c)
        >>> cb.pos = 5
        >>> cb.doDelete()
        >>> cb.getBuffer()
        (5, 'HalloWelt')
        >>> cb.pos = 0
        >>> cb.doDelete()
        >>> cb.getBuffer()
        (0, 'alloWelt')
        >>> cb.pos = 8
        >>> cb.doDelete()
        >>> cb.getBuffer()
        (8, 'alloWelt')
        """
        self.fixPos()
        if len(self.buffer) == 0:
            return# False
        if self.pos == len(self.buffer):
            return# False

        del self.buffer[self.pos]

        return# False

    def getBuffer(self):
        self.fixPos()
        return self.pos, "".join(self.buffer)
  
    def clear(self):
        """
        >>> cb = ConsoleBuffer()
        >>> for c in 'Hallo Welt': cb.append(c)
        >>> cb.clear()
        >>> cb.getBuffer()
        (0, '')
        """
        self.pos = 0
        self.buffer = []

    def append(self, data):
        """
        >>> cb = ConsoleBuffer()
        >>> cb.append('Hallo Welt')
        >>> cb.getBuffer()
        (10, 'Hallo Welt')
        >>> cb.append('')
        >>> cb.getBuffer()
        (10, 'Hallo Welt')
        """
        if len(data) > 1:
            for x in data:
                self.append(x)
        elif len(data) == 1:
            self.fixPos()
            self.buffer.insert(self.pos, data)
            self.pos += 1
        else:
            pass

    def set(self, data='Hallo Welt'):
        """
        >>> cb = ConsoleBuffer()
        >>> cb.set()
        >>> cb.getBuffer()
        (10, 'Hallo Welt')
        """
        self.clear()
        self.buffer = [x for x in data]
        self.pos = len(data)

if __name__ == "__main__":
    import doctest
    doctest.testmod()