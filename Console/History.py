class History(object):
    """
    >>> h = History(3)
    >>> h.append("1.")
    >>> h.append("2.")
    >>> h.append("3.")
    >>> h.append("4.")
    >>> for i in range(5): h.up()
    '4.'
    '3.'
    '2.'
    ''
    '4.'
    >>> for i in range(6): h.down()
    ''
    '2.'
    '3.'
    '4.'
    ''
    '2.'
    """
    def __init__(self, space = 50):
        self.space = space
        self.history =  []
        self.pos = 0

    def append(self, line):
        self.history.append(line)
        while len(self.history) > self.space:
            del self.history[0]
        self.pos = len(self.history)

    def up(self):
        self.pos -= 1
        return self.get()

    def down(self):
        self.pos += 1
        return self.get()
    
    def fixPos(self):
        if self.pos < 0:
            self.pos = len(self.history)
        if self.pos > len(self.history):
            self.pos = 0

    def get(self):
        self.fixPos()
        if self.pos == len(self.history):
            return ""
        return self.history[self.pos]


if __name__ == "__main__":
    import doctest
    doctest.testmod()