import Console

class ConsoleClient(object):
    """description of class"""
    def __init__(self, client, carrot = ">>> ", localsEnv=globals(), console = None):
        self.client = client
        self.carrot = carrot

        if not console:
            self.console = Console.customConsoleClass(localsEnv)
        else:
            self.console = console

        self.client.setCallback(self._dataCallback)

        self.sendCarrot()

    def _dataCallback(self, client, data):
        data = data.replace("\r", "").replace("\n", "")

        try:
            result = self.console.push(data)
        except Exception,ex:
            result = str(ex)

        print '"{0}" -> "{1}"'.format(data, result)
        
        if len(result) > 0:
            client.sendCommand(result)

        self.sendCarrot()

    def sendCarrot(self):
        self.client.send(self.carrot)

