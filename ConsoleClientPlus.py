import Console
import Console.ConsoleBuffer
import Console.History
from telnet import *
import traceback


TELNET_MODE = \
    IAC+WONT+ECHO+\
    IAC+WILL+SUPPRESS_GO_AHEAD+\
    IAC+WONT+LINEMODE

CURSOR_UP = "\x1b[A"
CURSOR_DOWN = "\x1b[B"
CURSOR_LEFT = "\x1b[D"
CURSOR_RIGHT = "\x1b[C"

CURSOR_BACKSTACE = "\x08"
CURSOR_DELETE = "\x1b[3~"

AUTOCOMPLETE = "\t"

NEWLINE = "\n"

class ConsoleClientPlus(object):
    """description of class"""
    def __init__(self, client, carrot = ">>> ", localsEnv=globals(), console = None):
        self.client = client
        self.carrot = carrot

        if not console:
            self.console = Console.customConsoleClass(localsEnv)
        else:
            self.console = console

        self.buffer = Console.ConsoleBuffer.ConsoleBuffer()
        self.history = Console.History.History()
        
        self.send(TELNET_MODE)
        self.sendCarrot()

        self.client.setCallback(self._dataCallback)


    def _dataCallback(self, client, data):
        #data = data.replace("\r", "").replace("\n", "")

        #print "Data :{0}".format(data.__repr__())

        push = False

        if data == "\r\x00" or data == "\r\n": # Enter
            push = True
        elif data == CURSOR_BACKSTACE: # Backspace
            self.buffer.doBackspace()
            self.updateLine()
        elif data.startswith("\x1b"): # Controlchar
            if data == CURSOR_DELETE: # Delete
                self.buffer.doDelete()
            elif data == CURSOR_LEFT: # Left
                self.buffer.doLeft()
            elif data == CURSOR_RIGHT: # Right
                self.buffer.doRight()
            elif data == CURSOR_UP: # Up
                self.send(CURSOR_DOWN)
                self.buffer.set(self.history.up())
            elif data == CURSOR_DOWN: # Down
                self.buffer.set(self.history.down())
            self.updateLine()
        elif data == AUTOCOMPLETE: # Tab
            pos, line = self.buffer.getBuffer()
            newText, printText = self.console.autocomplete(line, pos)
            if printText:
                self.send(NEWLINE)
                self.send(printText.__repr__()+NEWLINE)
                self.sendCarrot()
                self.send(line)
            else:
                self.send(CR)
                self.send(self.carrot + newText)
                self.buffer.set(newText)
            
        elif data[0] == "\xff":
            #print "Command: " + "+".join(parseCommands(data))
            pass
        else:
            self.buffer.append(data)

        #if data[0] != "\xff":
        #    self.client.send(data)

        if push:
            pos, line = self.buffer.getBuffer()
            self.buffer.clear()

            self.history.append(line)

            if line == "exit":
                self.client.close()
                return

            try:
                result = self.console.push(line)
            except:
                result = traceback.format_exc()

            #print '"{0}" -> "{1}"'.format(data, result)
        
            if len(result) > 0:
                client.sendCommand(NEWLINE+result)

            self.sendCarrot()


    def sendCarrot(self):
        self.client.send("\n"+self.carrot)

    def send(self, data):
        #print "SEND: "+data.__repr__()
        self.client.send(data)

    def updateLine(self):
        data = CR+(" "*80)+CR+self.carrot
        pos, line = self.buffer.getBuffer()
        data += line
        data += CURSOR_LEFT*(len(line)-pos)
        self.send(data)
