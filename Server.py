import socket
from thread import *
from Client import Client

class Server(object):
    """description of class"""
    def __init__(self, host, port, callback):
        self.host = host
        self.port = port
        self.callback = callback

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((host, port))
        self.socket.listen(10)

        self.active = True

        start_new_thread(self._readerloop ,())

        self.clients = []

    def _readerloop(self):
        # infinite loop so that function do not terminate and thread do not end.
        try:
            while self.active:
                conn, addr = self.socket.accept()
                client = Client(self, conn, addr, None, None)

                self.clients.append(client)

                if self.callback:
                    self.callback(self, client)
        except Exception,ex:
            print ex
        self.active = False

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.active = False
        self.socket.close()

        for c in self.clients:
            c.conn.close()

