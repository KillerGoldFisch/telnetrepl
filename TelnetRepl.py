#!/usr/bin/env python
# coding: utf8
 
"""TelnetRepl.py: A pure Python Telnet REPL Shell."""
 
__author__      = "Kevin Gliewe aka KillerGoldFisch"
__copyright__   = "Copyright 2015, Kevin Gliewe"
__credits__     = ["Kevin Gliewe",]
__license__     = "WTFPL"
__version__     = "1.0.0"
__date__        = "2015-03-16"
__maintainer__  = "Kevin Gliewe"
__email__       = "kevingliewe@gmail,com"
__status__      = "Production"
 
"""
 Copyright (C) 2015 Kevin Gliewe <kevingliewe@gmail.com>

 This program is free software. It comes without any warranty, to
 the extent permitted by applicable law. You can redistribute it
 and/or modify it under the terms of the Do What The Fuck You Want
 To Public License, Version 2, as published by Sam Hocevar. See
 http://www.wtfpl.net/ for more details.


            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
"""
 
import Server, time, ConsoleClientPlus
from thread import *

HOST = ''   # Symbolic name meaning all available interfaces
PORT = 23 # Arbitrary non-privileged port

class Auth(object):
    def __init__(self, localsEnv=globals()):
        self.localsEnv = localsEnv

    def checkCondition(self, username, password):
        return True

    def spawnConsole(self, username, client):
        return ConsoleClientPlus.ConsoleClientPlus(client, localsEnv=self.localsEnv)

    def auth(self, telnetRepl, server, client):
        client.send("username:")
        username = client.read(9999)
        if not username:
            client.close()
            return
        client.send("password:")
        password = client.read(9999)
        if not password:
            client.close()
            return
        username = username.translate(None, '\r\n')
        password = password.translate(None, '\r\n')

        if self.checkCondition(username, password):
            telnetRepl.clients.append(self.spawnConsole(username, client))
        else:
            client.close()
            return

class AuthDict(Auth):
    def __init__(self, authDict = dict(), localsEnv=globals()):
        super(AuthDict, self).__init__(localsEnv=localsEnv)
        self.authDict = authDict

    def checkCondition(self, username, password):
        try:
            return self.authDict[username] == password
        except:
            pass
        return False

class TelnetRepl(object):
    def __init__(self, host = HOST, port = PORT, auth = None, localsEnv=globals()):
        self.host = host
        self.port = port
        self.auth = auth
        self.localsEnv = localsEnv

        self.clients = []

        self.server = Server.Server(host, port, self.clientArrived)

    def clientArrived(self, server, client):
        if self.auth:
            start_new_thread(self.auth.auth ,(self, server, client,))
        else:
            self.clients.append(ConsoleClientPlus.ConsoleClientPlus(client, localsEnv=self.localsEnv))

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass


if __name__ == "__main__":
    import argparse
    def check_negative(value):
        if value == "":
            value = "0"
        ivalue = int(value)
        if ivalue < 0:
             raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
        return ivalue

    parser = argparse.ArgumentParser(description = 'Telnet REPL Python Console')
    parser.add_argument('-a', '--authfile',
                    dest="authfile",
                    help='File holding auth', 
                    default=None)
    parser.add_argument("-l", "--host",
                    type=check_negative, dest="host", default=HOST)
    parser.add_argument("-p", "--port",
                    type=check_negative, dest="port", default=PORT)
    args = vars(parser.parse_args())
    authfile = args['authfile']
    host = args['host']
    port = args['port']

    authdict = {}
    if authfile:
        with open(authfile) as f:
            content = f.readlines()
            for line in content:
                line = line.translate(None, '\r\n').split(" ")
                authdict[line[0]] = line[1]
    if authfile:
        tr = TelnetRepl(auth=AuthDict(authdict))
    else:
        tr = TelnetRepl()#auth=AuthDict({"user":"pass"}))
    while tr.server.active:
        time.sleep(1)